/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([], function () {
    return {
        collation: '0D050D060D070D080D090D0A0D0B0D600D0C0D610D0E0D0F0D100D120D130D140D150D7F0D160D170D180D190D1A0D1B0D1C0D1D0D1E0D1F0D200D210D220D230D7A0D240D250D260D270D280D7B0D290D2A0D2B0D2C0D2D0D2E0D2F0D300D4E0D7C0D320D7D0D350D360D370D380D390D330D7E0D340D310D3A0D3D0D3E0D3F0D400D410D420D430D440D620D630D460D470D480D46+0D3E0D4A0D47+0D3E0D4B0D46+0D570D4C0D570D4D',
        compare: 'multi'
    };
});

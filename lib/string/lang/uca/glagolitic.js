/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([], function () {
    return {
        collation: '2C302C002C312C012C322C022C332C032C342C042C352C052C362C062C372C072C382C082C392C092C3A2C0A2C3B2C0B2C3C2C0C2C3D2C0D2C3E2C0E2C3F2C0F2C402C102C412C112C422C122C432C132C442C142C452C152C462C162C472C172C482C182C492C192C4A2C1A2C4B2C1B2C4C2C1C2C4D2C1D2C4E2C1E2C4F2C1F2C502C202C512C212C522C222C532C232C542C242C552C252C562C262C572C272C582C282C592C292C5A2C2A2C5B2C2B2C5C2C2C2C5D2C2D2C5E2C2E',
        compare: 'single'
    };
});

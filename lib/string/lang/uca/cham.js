/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([
    "../single"
], function (Single) {
    return new Single({
        collation: 'AA00AA01AA02AA03AA04AA05AA06AA07AA08AA09AA0AAA0BAA0CAA0DAA0EAA0FAA10AA11AA12AA13AA14AA15AA16AA17AA18AA19AA1AAA1BAA1CAA1DAA1EAA1FAA20AA21AA22AA23AA24AA25AA26AA27AA28AA33AA34AA35AA36AA29AA2AAA2BAA2CAA2DAA2EAA2FAA30AA31AA32AA40AA41AA42AA43AA44AA45AA46AA47AA48AA49AA4AAA4BAA4CAA4D'
    });
});

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([], function () {
    return {
        collation: '0C850C860C870C880C890C8A0C8B0CE00C8C0CE10C8E0C8F0C900C920C930C940C950C960C970C980C990C9A0C9B0C9C0C9D0C9E0C9F0CA00CA10CA20CA30CA40CA50CA60CA70CA80CAA0CAB0CAC0CAD0CAE0CAF0CB00CB10CB20CB50CB60CB70CB80CB90CB30CDE0CBD0CF10CF20CBE0CBF0CBF+0CD50CC00CC10CC20CC30CC40CE20CE30CC60CC6+0CD50CC70CC6+0CD60CC80CC6+0CC20CCA0CC6+0CC2+0CD50CCA+0CD50CCB0CCC0CCD0CD50CD6',
        compare: 'multi'
    };
});

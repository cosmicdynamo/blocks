/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([], function () {
    return {
        collation: '0D850D860D870D880D890D8A0D8B0D8C0D8D0D8E0D8F0D900D910D920D930D940D950D960D9A0D9B0D9C0D9D0D9E0D9F0DA00DA10DA20DA30DA40DA50DA60DA70DA80DA90DAA0DAB0DAC0DAD0DAE0DAF0DB00DB10DB30DB40DB50DB60DB70DB80DB90DBA0DBB0DBD0DC00DC10DC20DC30DC40DC50DC60DCF0DD00DD10DD20DD30DD40DD60DD80DF20DDF0DF30DD90DD9+0DCA0DDA0DDB0DD9+0DCF0DDC0DD9+0DCF+0DCA0DDC+0DCA0DDD0DD9+0DDF0DDE0DCA',
        compare: 'multi'
    };
});

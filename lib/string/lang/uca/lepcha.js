/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define([], function () {
    return {
        collation: '1C001C011C021C031C041C051C061C071C081C091C4D1C4E1C4F1C0A1C0B1C0C1C0D1C0E1C0F1C101C111C121C131C141C151C161C171C181C191C1A1C241C1B1C251C1C1C1D1C1E1C1F1C201C211C221C231C361C261C271C281C291C2A1C2B1C2C1C2D1C2E1C2F1C301C311C321C331C341C35',
        compare: 'single'
    };
});

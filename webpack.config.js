/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require('webpack');
var argv = require('minimist')(process.argv.slice(2));
var path = require('path');


var DEBUG = !argv.release;


module.exports = {
  entry: {
    client: "./lib/index.js"
  },
  output: {
    path: './build/',
    publicPath: './build/',
    filename: "[name]" + (DEBUG ? "" : ".min") + ".js?[hash]"
  },
  cache: DEBUG,
  debug: DEBUG,
  devtool: DEBUG ? 'source-map' : false,
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel'
      },
      {test: /\.css$/, loader: "style-loader!css-loader"}
    ]
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      RdfJs: path.resolve("src/RdfJs"),
      blocks: path.resolve("src/blocks"),
      "nodice-redux": path.resolve("src/store")
    }
  },
  stats: {
    colors: true,
    reasons: DEBUG
  }
};

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var webpackConfig = require('./webpack.config.js');
webpackConfig.entry = {};
var path = require('path');

module.exports = function (config) {
  config.set({
    basePath: '.',
    frameworks: ['mocha', 'chai'],

    files: [
      'test.webpack.js'
    ],

    preprocessors: {
      'test.webpack.js': ['webpack', 'sourcemap'],
      'test/**/*.js': ['webpack', 'sourcemap'],
      'src/**/*.js': ['coverage', 'webpack', 'sourcemap']
    },

    webpack: { //kind of a copy of your webpack config + Istanbul
      devtool: 'source-map',
      module: {
        loaders: [
          {
            test: /.(jsx|js)?$/,
            loader: 'babel',
            query: {
              presets: ['es2015', 'stage-0'],
              sourceMap: 'inline'
            }
          }
        ],
        postLoaders: [{
          test: /\.js/,
          include: /(src)/,
          loader: 'istanbul-instrumenter'
        }]
      },
      resolve: {
        alias: {
          'blocks': path.resolve("./")
        }
      }
    },
    webpackServer: {
      noInfo: true //please don't spam the console when running in karma!
    },

    reporters: ['progress', 'coverage'],

    colors: true,
    autoWatch: false,

    browsers: [/*'PhantomJS',*/ 'Chrome'/*, 'IE'*/],

    // optionally, configure the reporter
    coverageReporter: {
      type: 'html',
      dir: 'reports/coverage/'
      /*,
       reporters: [
       // reporters not supporting the `file` property
       { type: 'html', subdir: 'report-html' },
       { type: 'lcov', subdir: 'report-lcov' },
       // reporters supporting the `file` property, use `subdir` to directly
       // output them in the `dir` directory
       { type: 'cobertura', subdir: '.', file: 'cobertura.txt' },
       { type: 'lcovonly', subdir: '.', file: 'report-lcovonly.txt' },
       { type: 'teamcity', subdir: '.', file: 'teamcity.txt' },
       { type: 'text', subdir: '.', file: 'text.txt' },
       { type: 'text-summary', subdir: '.', file: 'text-summary.txt' },
       ]
       */
    }
  })
};

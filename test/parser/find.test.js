/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { parser } from "blocks";
var { Data, find } = parser;
import returnPromise from "./resource/returnPromise";
import returnNull from "./resource/returnNull";
import returnValue from "./resource/returnValue";

describe("package - blocks", function () {
  describe("parser", function () {
    describe("find", function () {
      it("tries each parser until one returns a value", async function () {
        var data = new Data({
          input: "Pass"
        });

        var result = await find(data, [
          returnNull,
          returnValue
        ]);

        assert.strictEqual(result, data.input);
      });

      it("waits promise returned by parser", function () {
        var data = new Data({
          input: "Pass"
        });

        var ready = find(data, [
          returnPromise,
          returnValue
        ]);
        assert.notEqual(ready, data.input, "Went async to require modules");

        assert.isFunction(ready.then);

        return ready.then(function (result) {
          assert.strictEqual(result, data.input);
        });
      });
    });
  });
});

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { parser } from "blocks";
var { Data, anyKeyWord } = parser;

describe("package - blocks", function () {
  describe("parser", function () {
    describe("anyKeyWord", function () {
      it("returns the key word if it is next in the string", function () {
        var data = new Data({
          input: "The quick brown fox jumps over the lazy dog"
        });

        var out = anyKeyWord(data, ["NotThere", "THE"]);

        assert.strictEqual("THE", out);
        assert.strictEqual(3, data.pos);
      });

      it("returns the key word if it is next in the string", function () {
        var data = new Data({
          input: "The quick brown fox jumps over the lazy dog"
        });

        var out = anyKeyWord(data, ["NotThere", "THE"]);

        assert.strictEqual("THE", out);
        assert.strictEqual(3, data.pos);
      });

      it("returns the key word if it is next in the string", function () {
        var data = new Data({
          input: "The quick brown fox jumps over the lazy dog"
        });

        var out = anyKeyWord(data, ["NotThere", "THE"]);

        assert.strictEqual("THE", out);
        assert.strictEqual(3, data.pos);
      });

      it("returns null if it is not next in the string", function () {
        var data = new Data({
          input: " The quick brown fox jumps over the lazy dog"
        });

        var out = anyKeyWord(data, ["the", "The"]);

        assert.isNull(out);
        assert.strictEqual(0, data.pos);
      });

      describe("matchCase=true", function () {
        it("returns the key word if it is next in the string", function () {
          var data = new Data({
            input: "The quick brown fox jumps over the lazy dog"
          });

          var out = anyKeyWord(data, ["No", "The"], true);

          assert.strictEqual("The", out);
          assert.strictEqual(3, data.pos);
        });

        it("returns null if it is not next in the string", function () {
          var data = new Data({
            input: "The quick brown fox jumps over the lazy dog"
          });

          var out = anyKeyWord(data, ["ThE", "THE"], true);

          assert.isNull(out);
          assert.strictEqual(0, data.pos);
        });
      });

      describe("whiteSpace=true", function () {
        it("can ignore leading white space", function () {
          var data = new Data({
            input: " The quick brown fox jumps over the lazy dog",
            whiteSpace: function (data) {
              data.pos++;
            }
          });

          var out = anyKeyWord(data, ["the", "THE"], false, true);

          assert.strictEqual(out, "the");
        });
      });
    });
  });
});

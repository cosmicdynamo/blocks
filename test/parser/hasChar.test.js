/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { parser } from "blocks";
var { Data, hasChar } = parser;

describe("package - blocks", function () {
  describe("parser", function () {
    describe("hasChar", function () {
      it("Returns character if it appears in the input string", function () {
        var data = new Data({input: "$"});
        var out = hasChar(data, "$");

        assert.strictEqual(out, data.input);
        assert.isNull(hasChar(data, "$"), "position was updated");
      });

      describe("caseSensitive", function () {
        describe("undefined", function () {
          it("will not enforce case sensitivity", function () {
            var data = new Data({input: "a"});
            var out = hasChar(data, "A");

            assert.strictEqual("a", out);
          });
        });

        describe("true", function () {
          it("will respect case sensitivity", function () {
            var data = new Data({input: "a"});
            var out = hasChar(data, "A", true);
            assert.isNull(out);

            out = hasChar(data, "a", true);
            assert.strictEqual("a", out);
          });
        });

        describe("false", function () {
          it("will not enforce case sensitivity", function () {
            var data = new Data({input: "a"});
            var out = hasChar(data, "A");

            assert.strictEqual("a", out);
          });
        });
      });

      describe("whiteSpace", function () {
        describe("undefined", function () {
          it("Will not strip leading white space", function () {
            var data = new Data({input: " a"});
            data.whiteSpace = function (data) {
              data.pos++;
            };
            var out = hasChar(data, "A", false);

            assert.isNull(out);
          });
        });

        describe("true", function () {
          it("Will strip leading white space", function () {
            var data = new Data({input: " a"});
            data.whiteSpace = function (data) {
              data.pos++;
            };
            var out = hasChar(data, "A", false, true);

            assert.strictEqual("a", out);
          });
        });

        describe("false", function () {
          it("Will not strip leading white space", function () {
            var data = new Data({input: " a"});
            data.whiteSpace = function (data) {
              data.pos++;
            };
            var out = hasChar(data, "A", false);

            assert.isNull(out);
          });
        });
      });
    });
  });
});

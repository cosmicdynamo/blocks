/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Container } from "blocks";

describe("package - blocks", function () {
  describe("package - Container", function () {
    describe("get/set", function () {
      it("Able to set a value and re-get it", function () {
        var container = new Container();

        var v1 = "A String";
        var v2 = {an: "Object"};

        assert.isNull(container.get("field1"), "Does not have a value before it is set");
        assert.isNull(container.get("field2"), "Does not have a value before it is set");

        container.set("field1", v1);
        container.set("field2", v2);

        assert.strictEqual(container.get("field1"), v1, "Is now set to new value");
        assert.strictEqual(container.get("field2"), v2, "Is now set to new value");
      });
    });

    describe("keys", function () {
      it("returns the list of names in this Container", function () {
        var container = new Container();

        assert.strictEqual(0, container.keys().length, "Returns empty array before data is added");

        container.set("aColumn1", "value");
        container.set("aColumn2", "value");
        container.set("aColumn3", "value");
        container.set("aColumn4", "value");
        container.set("aColumn4", "SetAgain");

        var columns = container.keys();

        assert.strictEqual(4, columns.length, "Four Values were added");
        var values = {};
        columns.forEach(function (name) {
          values[name] = true;
        });

        assert.isTrue(values["aColumn1"], "Expected value Found");
        assert.isTrue(values["aColumn2"], "Expected value Found");
        assert.isTrue(values["aColumn3"], "Expected value Found");
        assert.isTrue(values["aColumn4"], "Expected value Found");
      });
    });

    describe("clone", function () {
      it("creates a new Container with the same values as the original", function () {
        var original = new Container();
        original.set("one", 1);
        original.set("two", 2);
        original.set("three", 3);
        original.set("four", 4);

        var clone = original.clone();

        assert.notEqual(original, clone, "It is not a reference to the same object");

        var keys = clone.keys();
        assert.strictEqual(keys.length, 4, "Correct number of values in Container");

        assert.strictEqual(clone.get("one"), 1, "Value 1 is correct");
        assert.strictEqual(clone.get("two"), 2, "Value 2 is correct");
        assert.strictEqual(clone.get("three"), 3, "Value 3 is correct");
        assert.strictEqual(clone.get("four"), 4, "Value 4 is correct");
      });
    });
  });
});

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Cache } from "blocks";

describe("package - blocks", function () {
  describe("package - Cache", function () {
    describe("get", function () {
      it("calls load method method if key not found", function () {
        var requestedId = null;
        var cache = new Cache({
          load: function (id) {
            requestedId = id;
          }
        });

        cache.get("anId");

        assert.strictEqual("anId", requestedId);
      });

      it("returns value from load method", function () {
        var cache = new Cache({
          load: function () {
            return "expected";
          }
        });

        var actual = cache.get("anId");

        assert.strictEqual("expected", actual);
      });

      it("load method is only called once, but the value is returned every time", function () {
        var called = 0;
        var cache = new Cache({
          load: function () {
            called++;
            return "expected";
          }
        });
        var actual = cache.get("anId");
        assert.strictEqual(1, called);
        assert.strictEqual("expected", actual);

        actual = cache.get("anId");
        assert.strictEqual(1, called);
        assert.strictEqual("expected", actual);
      });

      it("Supports Objects passed in as id values", function () {
        var called = 0;
        var cache = new Cache({
          load: function (id) {
            called++;
            return id.object.nested;
          }
        });

        var actual = cache.get({
          "one": "somewhere",
          "object": {"nested": "value1"},
          "value": "property"
        });
        assert.strictEqual(1, called, "load method called once");
        assert.strictEqual("value1", actual);

        actual = cache.get({
          "two": "somewhere",
          "object": {"nested": "value2"},
          "value": "property"
        });
        assert.strictEqual(called, 2, "load method called twice");
        assert.strictEqual("value2", actual);

        actual = cache.get({
          "one": "somewhere",
          "value": "property",
          "object": {"nested": "value1"}
        });
        assert.strictEqual(2, called, "load method still called only twice");
        assert.strictEqual("value1", actual);

        actual = cache.get({
          "two": "somewhere",
          "value": "property",
          "object": {"nested": "value2"}
        });
        assert.strictEqual(2, called, "load method called only twice");
        assert.strictEqual("value2", actual);
      });

      it("will recall load method if different key is passed in", function () {
        var called = [];
        var cache = new Cache({
          load: function (value) {
            called.push(value);
            return value;
          }
        });

        cache.get("id1");
        cache.get("id2");

        assert.strictEqual(2, called.length);

        assert.strictEqual("id1", called[0]);
        assert.strictEqual("id2", called[1]);
      });

      it("null return will prevent caching", function () {
        var called = 0;
        var cache = new Cache({
          load: function () {
            called++;
            return null;
          }
        });

        var actual = cache.get("anId");
        assert.strictEqual(1, called);
        assert.isNull(actual);

        actual = cache.get("anId");
        assert.strictEqual(2, called);
        assert.isNull(actual);

        cache.get("anId");
        assert.strictEqual(3, called);
      });
    });

    describe("remove", function () {
      it("clears a value from the cache", function () {
        var called = 0;
        var cache = new Cache({
          load: function () {
            return called++;
          }
        });

        called = 0;
        var key = "The Key";

        assert.strictEqual(0, cache.get(key));
        assert.strictEqual(0, cache.get(key));

        cache.remove(key);

        assert.strictEqual(1, cache.get(key));
      });
    });
  });
});
